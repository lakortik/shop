package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserPojo {


    private Integer id;
    private String firstName;
    private String lastName;
    private String patroName;
    private String login;

    private String password;
    private Integer accessLevel;

    public UserPojo(Integer id, String firstName, String password, Integer accessLevel){
        this.id = id;
        this.firstName = firstName;
        this.password = password;
        this.accessLevel = accessLevel;
    }
}
