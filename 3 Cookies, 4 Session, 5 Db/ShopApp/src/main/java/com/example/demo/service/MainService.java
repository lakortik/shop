package com.example.demo.service;

import com.example.demo.ConnectionHandler;
import com.example.demo.models.CityPojo;
import com.example.demo.models.ItemPojo;
import com.example.demo.models.RefOrdersItemsPojo;
import com.example.demo.models.UserPojo;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Service
public class MainService {
    public Integer changeItemCount(HttpSession session, Integer itemId, String actionType) throws SQLException {
        Integer orderId = getOrderIdBySessionId(session.getId());

        if (orderId == null) {
            orderId = insertOrder(session.getId());
        }

        RefOrdersItemsPojo refPojo = getRefOrdersItemsByOrderIdAndItemId(orderId, itemId);
        if (refPojo.getId() == null) {
            refPojo = insertRefOrdersItems(orderId, itemId);
        } else {
            //значит этот товар уже есть в рефке. надо изменить только его количество
            if (actionType.equals("addItem")){
                refPojo.setItemCount(refPojo.getItemCount() + 1);
            }
            if (actionType.equals("deleteItem")){
                refPojo.setItemCount(refPojo.getItemCount() - 1);
            }
            updateRefOrdersItems(refPojo);
        }

        return refPojo.getItemCount();
    }

    public void updateRefOrdersItems(RefOrdersItemsPojo refPojo) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE public.ref_orders_items " +
                        "SET  item_count=? " +//order_id=?, item_id=?,
                        "WHERE id = ?;"
        );
        preparedStatement.setInt(1, refPojo.getItemCount());
        preparedStatement.setInt(2, refPojo.getId());
        preparedStatement.execute();


        preparedStatement.close();
        connect.close();
    }

    public Integer insertOrder(String id) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "INSERT INTO public.orders(session_id)	" +
                        "VALUES (?) returning id;"
        );
        preparedStatement.setString(1, id);
        ResultSet result = preparedStatement.executeQuery();

        result.next();
        result.getRow();

        Integer ret = result.getInt("id");

        preparedStatement.close();
        connect.close();

        return ret;
    }

    public RefOrdersItemsPojo insertRefOrdersItems(Integer orderId, Integer itemId) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "INSERT INTO public.ref_orders_items(" +
                        "    order_id, item_id, item_count" +
                        ")" +
                        "VALUES (?, ?, ?) returning id;"
        );
        preparedStatement.setInt(1, orderId);
        preparedStatement.setInt(2, itemId);
        preparedStatement.setInt(3, 1);
        ResultSet result = preparedStatement.executeQuery();

        result.next();
        result.getRow();

        RefOrdersItemsPojo ret = new RefOrdersItemsPojo(
                result.getInt("id"),
                1
        );

        preparedStatement.close();
        connect.close();

        return ret;
    }

    public Integer getOrderIdBySessionId(String id) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement("SELECT id FROM public.orders where session_id = ?;");
        preparedStatement.setString(1, id);
        ResultSet result = preparedStatement.executeQuery();

        Integer ret = null;
        if (result.next()) {
            result.getRow();
            ret =  result.getInt("id");
        }

        preparedStatement.close();
        connect.close();

        return ret;

    }

    public RefOrdersItemsPojo getRefOrdersItemsByOrderIdAndItemId(Integer orderId, Integer itemId) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT id, item_count FROM public.ref_orders_items where order_id = ? and item_id = ?"
        );
        preparedStatement.setInt(1, orderId);
        preparedStatement.setInt(2, itemId);
        ResultSet result = preparedStatement.executeQuery();

        RefOrdersItemsPojo ret = new RefOrdersItemsPojo(null, null);
        if (result.next()) {
            result.getRow();
            ret = new RefOrdersItemsPojo(
                    result.getInt("id"),
                    result.getInt("item_count")
            );
        }

        preparedStatement.close();
        connect.close();

        return ret;

    }

    public List<ItemPojo> getItemList(String sid) throws SQLException {

        Integer orderId = getOrderIdBySessionId(sid);
        if (orderId == null){
            orderId = 0;//костыль. если заказа еще нет. даем значение по которому ничего не подтянет по суммам, а падать на null не будет
        }
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
            "SELECT items.id, items.name, items.description, items.price, items.category_id, " +
                " coalesce(" +
                    " (select ref_orders_items.item_count " +
                        "from public.ref_orders_items as ref_orders_items " +
                        "where ref_orders_items.order_id = ? " +
                        "and ref_orders_items.item_id = items.id " +
                    "), " +
                    "0 "+
                ") as bye_count " +
                " FROM public.items as items;"
        );

        preparedStatement.setInt(1, orderId);
        ResultSet result = preparedStatement.executeQuery();

        List<ItemPojo> itemPojos = new ArrayList<>();
        while(result.next()){
            itemPojos.add(new ItemPojo(
                    result.getInt("id"),
                    result.getString("name"),
                    result.getString("description"),
                    result.getInt("price"),
                    result.getInt("category_id"),
                    result.getInt("bye_count")
            ));
        }

        result.close();
        connect.close();

        return itemPojos;
    }

    public void confirmOrder(String sid, String fio, String address) throws SQLException {
        Integer orderId = getOrderIdBySessionId(sid);

        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE public.orders " +
                        "SET  address=? , fio = ? , bye_date_time = CURRENT_TIMESTAMP " +
                        "WHERE id = ?;"
        );
        preparedStatement.setString(1, address);
        preparedStatement.setString(2, fio);
        preparedStatement.setInt(3, orderId);
        preparedStatement.execute();


        preparedStatement.close();
        connect.close();

    }

    public List<CityPojo> getCityList() throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT items.id, items.name " +
                        " FROM public.d_city as items;"
        );

        ResultSet result = preparedStatement.executeQuery();

        List<CityPojo> cityPojoList = new ArrayList<>();
        while(result.next()){
            cityPojoList.add(new CityPojo(
                    result.getInt("id"),
                    result.getString("name")
            ));
        }

        result.close();
        connect.close();

        return cityPojoList;
    }

    private static final String SYMBOLS = "QWERTYUIOPasdfghjklZXCVBnm1234567890()!@#";
    private char getSymbol() {
        int index = (int) (Math.random() * SYMBOLS.length());
        return SYMBOLS.charAt(index);
    }
    public String generatePassword(int length) {
        StringBuilder sb = new StringBuilder();
        while (length != 0) {
            sb.append((char) getSymbol());
            length--;
        }
        return sb.toString();
    }

    public int changePassword(String sid, Integer id, String password, String newPassword) throws SQLException {
        UserPojo user = getUserBySessionId(sid);

        if (user.getId() == null
            || !user.getPassword().equals(md5Custom(password))
        ){
            return -1;//error разные пароли
        }

        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE public.users " +
                        "SET  password = ? " +
                        "WHERE id = ?;"
        );
        preparedStatement.setString(1, md5Custom(newPassword));
        preparedStatement.setInt(2, user.getId());
        preparedStatement.execute();

        preparedStatement.close();
        connect.close();

        return user.getId();
    }

    public String addUser(String newLogin, String firstName, String lastName, String patroName, Integer accessLevel) throws SQLException {

        Connection connect = ConnectionHandler.getDbConnection();

        String newPassword = generatePassword(8);
        PreparedStatement preparedStatement = connect.prepareStatement(
                "INSERT INTO public.users(" +
                        "    first_name, last_name, patro_name, access_level, password , login" +
                        ")" +
                        "VALUES (?, ?, ?, ?, ?,?) ;"
        );
        preparedStatement.setString(1, firstName);
        preparedStatement.setString(2, lastName);
        preparedStatement.setString(3, patroName);
        preparedStatement.setInt(4, accessLevel);
        preparedStatement.setString(5, md5Custom(newPassword));
        preparedStatement.setString(6, newLogin);
        preparedStatement.execute();

        preparedStatement.close();
        connect.close();

        return newPassword;
    }

    public int autentification(String sid, String login, String password) throws SQLException {
        UserPojo user = getUserByLogin(login);

        if (user.getId() == null
            || !user.getPassword().equals(md5Custom(password))
        ){
            return -1;//error пользователь не найден
        }

        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE public.users " +
                        "SET  session_id = ? " +
                        "WHERE id = ?;"
        );
        preparedStatement.setString(1, sid);
        preparedStatement.setInt(2, user.getId());
        preparedStatement.execute();

        preparedStatement.close();
        connect.close();

        return user.getId();
    }

    public void unAutentification(Integer id) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE public.users " +
                        "SET  session_id = null " +
                        "WHERE id = ?;"
        );
        preparedStatement.setInt(1, id);
        preparedStatement.execute();

        preparedStatement.close();
        connect.close();
    }

    public void deleteUser(Integer id) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "delete from public.users " +
                        "WHERE id = ?;"
        );
        preparedStatement.setInt(1, id);
        preparedStatement.execute();

        preparedStatement.close();
        connect.close();
    }

    public List<UserPojo> getUserList(String sid) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT users.id , users.first_name, users.last_name,users.login, users.patro_name, users.access_level" +
                        " FROM public.users as users;"
        );


        ResultSet result = preparedStatement.executeQuery();

        List<UserPojo> userPojoList = new ArrayList<>();
        while(result.next()){
            userPojoList.add(new UserPojo(
                    result.getInt("id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("patro_name"),
                    result.getString("login"),
                    "",
                    result.getInt("access_level")

            ));
        }

        result.close();
        connect.close();

        return userPojoList;
    }

    public UserPojo getUserBySessionId(String sid) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT users.id , users.first_name, users.access_level, users.password " +
                        "FROM public.users as users " +
                        "where session_id = ?;"
        );
        preparedStatement.setString(1, sid);
        ResultSet result = preparedStatement.executeQuery();

        UserPojo ret = new UserPojo();
        if (result.next()) {
            result.getRow();
            ret = new UserPojo(
                    result.getInt("id"),
                    result.getString("first_name"),
                    result.getString("password"),
                    result.getInt("access_level")
            );
        }

        preparedStatement.close();
        connect.close();

        return ret;
    }

    public UserPojo getUserByLogin(String login) throws SQLException {
        Connection connect = ConnectionHandler.getDbConnection();

        PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT users.id , users.first_name, users.password " +
                        "FROM public.users as users " +
                        "where users.login = ?;"
        );
        preparedStatement.setString(1, login);
        ResultSet result = preparedStatement.executeQuery();

        UserPojo ret = new UserPojo();
        if (result.next()) {
            result.getRow();
            ret = new UserPojo(
                    result.getInt("id"),
                    result.getString("first_name"),
                    result.getString("password"),
                    null
            );
        }

        preparedStatement.close();
        connect.close();

        return ret;
    }

    public String md5Custom(String st) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            // тут можно обработать ошибку
            // возникает она если в передаваемый алгоритм в getInstance(,,,) не существует
            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while( md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }

        return md5Hex.toString();
    }

    public Workbook exportUsers() throws SQLException {
        List<UserPojo> resultItems = getUserList("");

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        ExportHelper.setColumnWidth(sheet, new Integer[]{1000, 5000, 5000, 5000, 5000, 1000, 5000});

        CellStyle style = ExportHelper.createStyle(workbook);
        ExportHelper.createHeadRow(sheet,
                new String[]{
                        "ID",
                        "Имя",
                        "Фамилия",
                        "Отчество",
                        "Уровень доступа",
                        "Логин",
                });

        for (int i = 0; i < resultItems.size(); i++) {
            Row dataRow = sheet.createRow(i+1);
            int index = 0;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getId() != null ? resultItems.get(i).getId() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getFirstName() != null ? resultItems.get(i).getFirstName() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getLastName() != null ? resultItems.get(i).getLastName() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getPatroName() != null ? resultItems.get(i).getPatroName() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getAccessLevel() != null ? resultItems.get(i).getAccessLevel() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getLogin() != null ? resultItems.get(i).getLogin() : "");
            index++;

        }
        return workbook;

    }
    
    public Workbook exportItems() throws SQLException {
        List<ItemPojo> resultItems = getItemList("");

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        ExportHelper.setColumnWidth(sheet, new Integer[]{1000, 5000, 5000, 5000});
        CellStyle style = ExportHelper.createStyle(workbook);
        ExportHelper.createHeadRow(sheet,
                new String[]{
                        "ID",
                        "Название",
                        "Описание",
                        "Цена"
                });

        for (int i = 0; i < resultItems.size(); i++) {
            Row dataRow = sheet.createRow(i+1);
            int index = 0;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getId());
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getName() != null ? resultItems.get(i).getName() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getDescription() != null ? resultItems.get(i).getDescription() : "");
            index++;

            ExportHelper.createDataCell(dataRow, index, CellType.STRING, style, resultItems.get(i).getPrice() );
            index++;


        }
        return workbook;

    }
}
