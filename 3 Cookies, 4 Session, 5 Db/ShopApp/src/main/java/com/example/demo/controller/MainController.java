package com.example.demo.controller;

import com.example.demo.models.CityPojo;
import com.example.demo.models.ItemPojo;
import com.example.demo.models.UserPojo;
import com.example.demo.service.MainService;
import com.google.gson.Gson;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainController {

    @Autowired
    private MainService mainService;

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");

    @RequestMapping("/changeItemCount")
    public void changeItemCount(
            @RequestParam(name = "itemId", required = false, defaultValue = "") Integer itemId,
            @RequestParam(name = "operationType", required = false, defaultValue = "") String operationType,
            HttpSession session,
            HttpServletResponse response
    ) throws IOException, SQLException {
        Integer newItemCount = mainService.changeItemCount(session, itemId, operationType);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        PrintWriter out = response.getWriter();
        out.write(String.valueOf(newItemCount));
        out.flush();
    }

    @RequestMapping("/sendOrder")
    public void sendOrder(
            @RequestParam(name = "fio", required = false, defaultValue = "") String fio,
            @RequestParam(name = "address", required = false, defaultValue = "") String address,
            HttpSession session,
            HttpServletResponse response,
            @CookieValue("cityId") Integer cityId
    ) throws IOException, SQLException {
        String sid = session.getId();
        String cityName = mainService.getCityList().stream().filter(i -> i.getId() == cityId.longValue()).findFirst().get().getName();
        mainService.confirmOrder(sid, fio, "город " + cityName + " " + address);

        PrintWriter out = response.getWriter();
        out.write(String.valueOf(1));
        out.flush();
    }



    @RequestMapping("/")
    public ModelAndView generalPage(HttpSession session) throws SQLException {
        ModelAndView mav = new ModelAndView("index");

        String sid = session.getId();

        List<ItemPojo> itemList = mainService.getItemList(sid);
        List<CityPojo> cityList = mainService.getCityList();

        Gson gson = new Gson();

        mav.addObject("itemList", gson.toJson(itemList));
        mav.addObject("cityList", gson.toJson(cityList));

        return mav; //general view
    }

    @RequestMapping("/bye")
    public ModelAndView byePage(HttpSession session) throws SQLException {
        ModelAndView mav = new ModelAndView("bye");

        String sid = session.getId();

        List<ItemPojo> itemList = mainService.getItemList(sid).stream().filter(i -> i.getByeCount() > 0).collect(Collectors.toList());
        List<CityPojo> cityList = mainService.getCityList();

        Gson gson = new Gson();

        mav.addObject("itemList", gson.toJson(itemList));
        mav.addObject("cityList", gson.toJson(cityList));

        return mav; //general view
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/city")
    public ResponseEntity<List<CityPojo>> getCityList() throws SQLException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(mainService.getCityList());

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/items")
    public ResponseEntity<List<ItemPojo>> getList(HttpSession session) throws SQLException {
        String sid = session.getId();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(mainService.getItemList(sid));

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/changeItemCountCros")
    public ResponseEntity<Integer> changeItemCountCros(
            @RequestParam(name = "itemId", required = false, defaultValue = "") Integer itemId,
            @RequestParam(name = "operationType", required = false, defaultValue = "") String operationType,
            HttpSession session
//            HttpServletResponse response
    ) throws IOException, SQLException {

        Integer newItemCount = mainService.changeItemCount(session, itemId, operationType);


        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(newItemCount);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/sendOrder")
    public ResponseEntity<Integer> sendOrderCros(
            @RequestParam(name = "fio", required = false, defaultValue = "") String fio,
            @RequestParam(name = "address", required = false, defaultValue = "") String address,
            HttpSession session,
            HttpServletResponse response,
            @CookieValue("cityId") Integer cityId
    ) throws IOException, SQLException {
        String sid = session.getId();
        String cityName = mainService.getCityList().stream().filter(i -> i.getId() == cityId.longValue()).findFirst().get().getName();
        mainService.confirmOrder(sid, fio, "город " + cityName + " " + address);


        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(1);

    }






    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/autentification")
    public ResponseEntity<Integer> autentification(
            @RequestParam(name = "login", required = false, defaultValue = "") String login,
            @RequestParam(name = "password", required = false, defaultValue = "") String password,
            HttpSession session,
            HttpServletResponse response
    ) throws IOException, SQLException {
        String sid = session.getId();
        int codeProcess = mainService.autentification(sid, login, password);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(codeProcess);

    }



    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/unAutentification")
    public ResponseEntity<Integer> unAutentification(
            @RequestParam(name = "id", required = false, defaultValue = "") Integer id
    ) throws IOException, SQLException {
        mainService.unAutentification(id);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(1);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/changePassword")
    public ResponseEntity<Integer> changePassword(
            @RequestParam(name = "id", required = false, defaultValue = "") Integer id,
            @RequestParam(name = "password", required = false, defaultValue = "") String password,
            @RequestParam(name = "newPassword", required = false, defaultValue = "") String newPassword,
            HttpSession session,
            HttpServletResponse response
    ) throws IOException, SQLException {
        String sid = session.getId();

        int codeProcess = mainService.changePassword(sid, id, password, newPassword);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(codeProcess);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/addUser")
    public ResponseEntity<String> addUser(
            @RequestParam(name = "newLogin", required = false, defaultValue = "") String newLogin,
            @RequestParam(name = "firstName", required = false, defaultValue = "") String firstName,
            @RequestParam(name = "lastName", required = false, defaultValue = "") String lastName,
            @RequestParam(name = "patroName", required = false, defaultValue = "") String patroName,
            @RequestParam(name = "accessLevel", required = false, defaultValue = "") Integer accessLevel,
            HttpSession session,
            HttpServletResponse response
    ) throws IOException, SQLException {
        String newPassword = mainService.addUser(newLogin, firstName, lastName, patroName, accessLevel);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(newPassword);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/deleteUser")
    public ResponseEntity<Integer> deleteUser(
            @RequestParam(name = "id", required = false, defaultValue = "") Integer id
    ) throws IOException, SQLException {
        mainService.deleteUser(id);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials",
                "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(1);

    }



    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/users")
    public ResponseEntity<List<UserPojo>> getListUser(HttpSession session) throws SQLException {
        String sid = session.getId();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials", "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(mainService.getUserList(sid));

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/isAutorize")
    public ResponseEntity<UserPojo> isAutorize(HttpSession session) throws SQLException {
        String sid = session.getId();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Credentials", "true");

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(mainService.getUserBySessionId(sid));

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/exportUsers")
    public void exportUsers(HttpServletResponse response) throws IOException {
        try (Workbook book = mainService.exportUsers()) {
            String fileName = "Отчет о пользователях.xlsx";
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8").replace("+", "%20"));
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            book.write(response.getOutputStream());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/api/exportItems")
    public void exportItems(HttpServletResponse response) throws IOException {
        try (Workbook book = mainService.exportItems()) {
            String fileName = "Отчет о товарах.xlsx";
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Expires", "0");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8").replace("+", "%20"));
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            book.write(response.getOutputStream());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
