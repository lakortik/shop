package com.example.demo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefOrdersItemsPojo {

    private Integer id;
    private Integer itemCount;

    public RefOrdersItemsPojo(Integer id, Integer item_count) {
        this.id = id;
        this.itemCount = item_count;
    }

}
