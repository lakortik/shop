package com.example.demo.service;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.Units;
import org.apache.poi.wp.usermodel.HeaderFooterType;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.stereotype.Service;


import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public abstract class ExportHelper {

    //Установка ширины столбцов
    public static void setColumnWidth(Sheet sheet, Integer[] widthArray){
        for (int i = 0; i < widthArray.length; i++) {
            sheet.setColumnWidth(i, widthArray[i]);
        }
    }

    //Создание стиля для заголовков
    public static CellStyle createHeaderStyle(Workbook workbook){
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setWrapText(true);
        headerStyle.setFont(headerFont);
        headerStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.index);
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        headerStyle.setFillPattern(FillPatternType.BIG_SPOTS);
        return headerStyle;
    }

    //Создание общего стиля
    public static CellStyle createStyle(Workbook workbook){
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        return style;
    }

    //Создание стиля для денежных ячеек
    public static CellStyle createCurrencyStyle(Workbook workbook){
        CellStyle currencyStyle = workbook.createCellStyle();
        DataFormat currencyFormat = workbook.createDataFormat();
        currencyStyle.setDataFormat(currencyFormat.getFormat("### ### ### ### ##0.00"));
        currencyStyle.setAlignment(HorizontalAlignment.CENTER);
        currencyStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        currencyStyle.setBorderBottom(BorderStyle.THIN);
        currencyStyle.setBorderLeft(BorderStyle.THIN);
        currencyStyle.setBorderRight(BorderStyle.THIN);
        currencyStyle.setBorderTop(BorderStyle.THIN);
        return currencyStyle;
    }

    //Создание хедера с именами столбцов
    public static void createHeadRow(Sheet sheet, String[] headerNames){
        createHeadRow(sheet, 0, headerNames);
    }

    //Создание хедера с именами столбцов
    public static void createHeadRow(Sheet sheet, int index, String[] headerNames){
        Workbook workbook = sheet.getWorkbook();
        CellStyle headerStyle = createHeaderStyle(workbook);
        Row headRow = sheet.createRow(index);
        for (int i = 0; i < headerNames.length; i++) {
            Cell headCell = headRow.createCell(i, CellType.STRING);
            headCell.setCellStyle(headerStyle);
            headCell.setCellValue(headerNames[i]);
        }
    }
    //Создание data ячейки.
    public static void createDataCell(Row dataRow, int index, CellType type, CellStyle style, Object value){
        Cell cell = dataRow.createCell(index, type);
        cell.setCellStyle(style);
        switch (type){
            case STRING:{
                if (value != null){
                    if(value instanceof String) {
                        cell.setCellValue((String)value);
                    } else {
                        cell.setCellValue(value.toString());
                    }
                }
                break;
            }
            case NUMERIC:{
                if (value instanceof Double) {
                    cell.setCellValue((Double)value);
                } else if (value instanceof BigDecimal) {
                    cell.setCellValue(((BigDecimal)value).doubleValue());
                } else if (value instanceof Short) {
                    cell.setCellValue(((Short)value).doubleValue());
                } else if (value instanceof Long) {
                    cell.setCellValue(((Long)value).doubleValue());
                } else {
                    cell.setCellValue(0.0);
                }
                break;
            }
            default:{
                break;
            }
        }
    }

    public static String getStringValueFromCell(XSSFCell cell) {
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                return Double.toString(cell.getNumericCellValue());
            default:
                return "";
        }
    }

    public static Rectangle2D getStringBounds(Font cellFont, String line){
        AffineTransform affinetransform = new AffineTransform();
        FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
        java.awt.Font font = new java.awt.Font(cellFont.getFontName(),
                cellFont.getBold() ? java.awt.Font.BOLD : java.awt.Font.PLAIN,
                cellFont.getBold() ? cellFont.getFontHeightInPoints() + 1 : cellFont.getFontHeightInPoints());

        return font.getStringBounds(line, frc);
    }

    public static void autoSizeRow(XSSFSheet sheet, int rowNum, int fromCol, int toCol) {
        XSSFRow row = sheet.getRow(rowNum);
        int maxLinesNum = 0;
        int maxFontSize = 0;
        float maxHeight = 0;
        float finalRowSize = 0;
        for (int col = fromCol; col <= toCol; col++) {
            XSSFCell cell = row.getCell(col);
            float finalCellSize = 0;

            if (cell == null || getStringValueFromCell(cell).length() == 0) {
                continue;
            }
            Font cellFont = cell.getCellStyle().getFont();
            //Устранение символа переноса строки в конце текста
            if(getStringValueFromCell(cell).charAt(getStringValueFromCell(cell).length()-1) == '\n') {
                cell.setCellValue(getStringValueFromCell(cell).substring(0, getStringValueFromCell(cell).length()-1));
            }

            // Получение всех строк по отдельности
            String[] lines = getStringValueFromCell(cell).split("\n");
            int numberOfMergedCells = 1;
            int linesNum = 0;

            for(CellRangeAddress oneRange : sheet.getMergedRegions()) {
                if(oneRange.isInRange(cell)) {
                    numberOfMergedCells = oneRange.getLastColumn() - oneRange.getFirstColumn() + 1;
                    break;
                }
            }
            float columnWidth = 0;
            for (int j = cell.getColumnIndex(); j < cell.getColumnIndex() + numberOfMergedCells; j++){
                columnWidth += sheet.getColumnWidthInPixels(j);
            }

            for (String line : lines){
                linesNum++;
                Rectangle2D lineBounds = getStringBounds(cellFont, line);
                float lineWidth = (float)(lineBounds.getWidth()) / 72 * 96; //переход к размеру при 96 dpi
                float lineHeight = (float)(lineBounds.getHeight()) / 72 * 96 + 3; //3 ~ кол-во пикселей между строками
                finalCellSize += lineHeight;
                maxHeight = Math.max(maxHeight, lineHeight);
                if (lineWidth > columnWidth){
                    String[] words = line.split(" ");
                    float spaceWidth = (float)getStringBounds(cellFont, " ").getWidth() / 72 * 96;
                    float tempColWidth = columnWidth;
                    for (String word : words) {
                        Rectangle2D wordBounds = getStringBounds(cellFont, word);
                        float wordWidth = spaceWidth + (float)wordBounds.getWidth() / 72 * 96;
                        if (Math.floor(wordWidth) < tempColWidth) {
                            tempColWidth -= wordWidth;
                        } else {
                            if (Math.floor(wordWidth) <= columnWidth){
                                linesNum++;
                                tempColWidth = columnWidth - (wordWidth + spaceWidth);
                                finalCellSize += lineHeight;
                            } else {
                                finalCellSize = finalCellSize + (int)Math.floor(wordWidth / columnWidth) * lineHeight;
                                linesNum = linesNum + (int)Math.floor(wordWidth / columnWidth);
                                tempColWidth = columnWidth - (wordWidth % columnWidth);
                            }
                        }
                    }
                }
            }
            maxFontSize = Math.max(maxFontSize, cellFont.getFontHeightInPoints());
            maxLinesNum = Math.max(linesNum, maxLinesNum);
            finalRowSize = Math.max(finalRowSize, finalCellSize);
        }
        finalRowSize *= 0.75f; //переход от пикселей к экселевским попугаям

        sheet.getRow(rowNum).setHeightInPoints(finalRowSize);
    }

    public static void autoSizeColumn(XSSFSheet sheet, int colNum, int fromRow, int toRow) {
        int maxWidth = 0;
        int maxFontSize = 0;
        for (int row = fromRow; row <= toRow; row++) {
            XSSFCell cell = sheet.getRow(row).getCell(colNum);
            XSSFFont cellFont = cell.getCellStyle().getFont();

            if (cell == null || cell.getStringCellValue().length() == 0) {
                continue;
            }
            //Устранение символа переноса строки в конце текста
            if(cell.getStringCellValue().charAt(cell.getStringCellValue().length()-1) == '\n') {
                cell.setCellValue(cell.getStringCellValue().substring(0, cell.getStringCellValue().length()-1));
            }

            // Получение всех строк по отдельности
            String line = cell.getStringCellValue();
            AffineTransform affinetransform = new AffineTransform();
            FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
            java.awt.Font font = new java.awt.Font(cellFont.getFontName(),
                    cellFont.getBold() ? java.awt.Font.BOLD : java.awt.Font.PLAIN,
                    cellFont.getBold() ? cellFont.getFontHeightInPoints() + 1 : cellFont.getFontHeightInPoints());

            int lineWidth = (int)(font.getStringBounds(line, frc).getWidth());
            maxWidth = Math.max(lineWidth, maxWidth);

            maxFontSize = Math.max(maxFontSize, cellFont.getFontHeightInPoints());
        }

        sheet.setColumnWidth(colNum, maxWidth * maxFontSize * 4);
    }

    public static void createMergedRegion(Sheet sheet, CellStyle cellStyle, int firstRow, int lastRow, int firstCol, int lastCol) {
        for (int i=firstRow; i<=lastRow; i++) {
            if (sheet.getRow(i) == null) {
                sheet.createRow(i);
            }
            for (int j=firstCol; j<=lastCol; j++) {
                if (sheet.getRow(i).getCell(j) == null) {
                    sheet.getRow(i).createCell(j);
                }
                if (cellStyle != null) {
                    sheet.getRow(i).getCell(j).setCellStyle(cellStyle);
                }
            }
        }
        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
    }

    public static void setCellText(Sheet sheet, int row, int col, String value) {
        setCellText(sheet, row, col, value, null);
    }

    public static void setCellText(Sheet sheet, int row, int col, String value, CellStyle style) {
        if (sheet.getRow(row) == null) {
            sheet.createRow(row);
        }
        if (sheet.getRow(row).getCell(col) == null) {
            sheet.getRow(row).createCell(col);
        }
        sheet.getRow(row).getCell(col).setCellValue(value);
        if (style != null) {
            sheet.getRow(row).getCell(col).setCellStyle(style);
        }
    }

    public void setCellText(Cell cell, String value, CellStyle style) {
        if (cell != null) {
            cell.setCellStyle(style);
            cell.setCellValue(value);
        }
    }

    public static byte[] getBytesFromWorkbook(Workbook workbook) {
        byte[] res = null;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            workbook.write(bos);
            res = bos.toByteArray();
        } catch (IOException e) {
        }
        return res;
    }

    // ----------------------- Word -----------------------


    public static void addBrakeInCell(XWPFTable table, int row, int col) {
        table.getRow(row).getCell(col).getParagraphs().get(0).createRun().addBreak();
    }

    public static void addBordersInCell(XWPFTable table, int row, int col, STBorder.Enum top, STBorder.Enum bottom, STBorder.Enum left, STBorder.Enum right) {
        CTTc ctTc = table.getRow(row).getCell(col).getCTTc();
        CTTcPr ctTcPr = ctTc.isSetTcPr() ? ctTc.getTcPr() : ctTc.addNewTcPr();
        CTTcBorders ctTcBorders = ctTcPr.isSetTcBorders() ? ctTcPr.getTcBorders() : ctTcPr.addNewTcBorders();
        CTBorder topBorder = ctTcBorders.isSetTop() ? ctTcBorders.getTop() : ctTcBorders.addNewTop();
        CTBorder bottomBorder = ctTcBorders.isSetBottom() ? ctTcBorders.getBottom() : ctTcBorders.addNewBottom();
        CTBorder leftBorder = ctTcBorders.isSetLeft() ? ctTcBorders.getLeft() : ctTcBorders.addNewLeft();
        CTBorder rightBorder = ctTcBorders.isSetRight() ? ctTcBorders.getRight() : ctTcBorders.addNewRight();

        topBorder.setVal(top);
        bottomBorder.setVal(bottom);
        leftBorder.setVal(left);
        rightBorder.setVal(right);
    }

    public static void setTableColumnWidth(XWPFTable table, int col, int width, boolean inPercents) {
        List<XWPFTableRow> rows = table.getRows();
        for (XWPFTableRow row : rows) {
            CTTc ctTc = row.getCell(col).getCTTc();
            if (ctTc.getTcPr() == null) {
                ctTc.addNewTcPr();
            }
            if (ctTc.getTcPr().getTcW() == null) {
                ctTc.getTcPr().addNewTcW();
            }
            if (inPercents) {
                ctTc.getTcPr().getTcW().setType(STTblWidth.PCT);
            }
            ctTc.getTcPr().getTcW().setW(BigInteger.valueOf(width));
        }
    }

    public static void mergeCellsHorizontal(XWPFTableRow row, int fromCell, int toCell) {
        CTHMerge hMerge = CTHMerge.Factory.newInstance();
        hMerge.setVal(STMerge.RESTART);
        CTHMerge hMerge2 = CTHMerge.Factory.newInstance();
        hMerge2.setVal(STMerge.CONTINUE);

        if (row.getCell(fromCell).getCTTc().getTcPr() == null) {
            row.getCell(fromCell).getCTTc().addNewTcPr();
        }
        row.getCell(fromCell).getCTTc().getTcPr().setHMerge(hMerge);

        for (int i = fromCell + 1; i <= toCell; i++) {
            CTTc ctTc = row.getCell(i).getCTTc();
            if (ctTc.getTcPr() == null) {
                ctTc.addNewTcPr();
            }
            ctTc.getTcPr().setHMerge(hMerge2);
        }
    }

    public static void mergeCellsVertical(XWPFTable table, int col, int fromRow, int toRow){
        CTTc ctTc = table.getRow(fromRow).getCell(col).getCTTc();
        CTTcPr ctTcPr = ctTc.isSetTcPr() ? ctTc.getTcPr() : ctTc.addNewTcPr();
        CTVMerge ctvMerge = ctTcPr.isSetVMerge() ? ctTcPr.getVMerge() : ctTcPr.addNewVMerge();
        ctvMerge.setVal(STMerge.RESTART);

        for (int i = fromRow + 1; i <= toRow; i++) {
            CTTc ctTc2 = table.getRow(i).getCell(col).getCTTc();
            CTTcPr ctTcPr2 = ctTc2.isSetTcPr() ? ctTc2.getTcPr() : ctTc2.addNewTcPr();
            CTVMerge ctvMerge2 = ctTcPr2.isSetVMerge() ? ctTcPr2.getVMerge() : ctTcPr2.addNewVMerge();
            ctvMerge2.setVal(STMerge.CONTINUE);
        }
    }

    // Page Size - a4
    public static void changeOrientation(XWPFDocument document, String orientation, boolean isFinalSectionInDocument){
        CTSectPr section;
        if (isFinalSectionInDocument) {
            CTDocument1 doc = document.getDocument();
            CTBody body = doc.getBody();
            section = body.addNewSectPr();
        } else {
            XWPFParagraph para = document.createParagraph();
            CTP ctp = para.getCTP();
            CTPPr br = ctp.addNewPPr();
            section = br.addNewSectPr();
            br.setSectPr(section);
        }
        CTPageSz pageSize = section.isSetPgSz() ? section.getPgSz() : section.addNewPgSz();
        if (orientation.equals("landscape")) {
            pageSize.setOrient(STPageOrientation.LANDSCAPE);
            pageSize.setW(BigInteger.valueOf(842*20));
            pageSize.setH(BigInteger.valueOf(595*20));
        }
        else {
            pageSize.setOrient(STPageOrientation.PORTRAIT);
            pageSize.setH(BigInteger.valueOf(842*20));
            pageSize.setW(BigInteger.valueOf(595*20));
        }
    }



    public static void setPageMargin(XWPFDocument doc, BigInteger top, BigInteger bottom, BigInteger left, BigInteger right, boolean isFinalSectionInDocument) {
        CTPageMar ctPageMar;
        if (isFinalSectionInDocument) {
            CTSectPr docCtSectPr = doc.getDocument().getBody().isSetSectPr() ? doc.getDocument().getBody().getSectPr() : doc.getDocument().getBody().addNewSectPr();
            ctPageMar = docCtSectPr.isSetPgMar() ? docCtSectPr.getPgMar() : docCtSectPr.addNewPgMar();
        } else {
            CTPPr ctpPr = doc.getLastParagraph().getCTP().isSetPPr() ? doc.getLastParagraph().getCTP().getPPr() : doc.getLastParagraph().getCTP().addNewPPr();
            CTSectPr ctSectPr = ctpPr.isSetSectPr() ? ctpPr.getSectPr() : ctpPr.addNewSectPr();
            ctPageMar = ctSectPr.isSetPgMar() ? ctSectPr.getPgMar() : ctSectPr.addNewPgMar();
        }

        ctPageMar.setTop(top);
        ctPageMar.setBottom(bottom);
        ctPageMar.setLeft(left);
        ctPageMar.setRight(right);
    }



    // Установка размеров столбца таблицы
    public static void setSizeColumn(XWPFTable table, int column, int size) {

        for (XWPFTableRow row : table.getRows()) {
            XWPFTableCell cell = row.getCell(column);
            CTTcPr tcpr = cell.getCTTc().addNewTcPr();

            CTTblWidth ctTblWidth = tcpr.addNewTcW();
            ctTblWidth.setW(BigInteger.valueOf(size));
        }
    }
}
