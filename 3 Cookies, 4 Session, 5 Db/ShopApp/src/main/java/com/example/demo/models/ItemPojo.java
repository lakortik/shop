package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ItemPojo {


    private int id;
    private String name;
    private String description;
    private int price;
    private int categoryId;
    private Integer byeCount;
}
