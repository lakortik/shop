//Modal component
import React, {useState} from 'react';
import Modal, { ModalHeader, ModalBody, ModalFooter } from '../components/modal';

function CityModal(
    {props: {
        isOpen,
        cityList,
        cityId,
        toggle,
    }},
) {

    const [newCityId, setNewCityId] = useState(cityId);

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader>
                <h3>Выберите ваш город
                </h3>
                <button
                    type="button"
                    className="close"
                    aria-label="Close"
                    onClick={toggle}
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </ModalHeader>
            <ModalBody>

                <select className="w100"
                        onClick={({target: {value}}) => setNewCityId(parseInt(value))}
                >
                    {
                        cityList.map(({id, name}, key) => <option key = {key}
                        value={id}
                    >
                        {name}
                    </option>
                    )}
                </select>

            </ModalBody>
            <ModalFooter>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={toggle}
                >
                    Закрыть
                </button>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                        document.cookie = `cityId=${newCityId || 1}` ;
                        document.location.reload();
                        // toggle();
                    }}
                >
                    Выбрать
                </button>
            </ModalFooter>
        </Modal>
    );
}

export default CityModal;