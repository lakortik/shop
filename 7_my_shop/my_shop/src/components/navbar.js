import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faMap, faHeartbeat} from '@fortawesome/free-solid-svg-icons'



function Navbar(
    {props: {
        toggle,
        cityList,
        cityId,
        loginUserId,
        userFio,
        accessLevel,

        setPage,
        unAutentification
    }},
) {


  return (
    <>
          <nav className="site-header sticky-top py-4 head-navbar px-0 navbar navbar-expand-lg navbar-light"
          >

            <div className="container-fluid px-0">

              <div className="col-7 offset-1 px-0">
                <div className="collapse navbar-collapse" id="navbarNav">
                    {accessLevel !== 3 && (

                              <a className="navbar-brand text-white mr-5 " href="#">
                                <FontAwesomeIcon icon= {faHeartbeat} />
                                 Аптека
                              </a>
                    )}

                    <ul className="navbar-nav">
                    <li className="nav-item">
                      <a className="nav-link mx-4 text-white" href="#"
                         onClick={() => setPage('main')}>
                          Главная
                      </a>
                    </li>

                      <li className="nav-item">
                          <a className="nav-link mx-4 text-white" href="#"
                             onClick={() => setPage('bye')}>
                              Корзина
                          </a>
                      </li>

                      {accessLevel === 3 && (
                          <li className="nav-item">
                              <a className="nav-link mx-4 text-white" href="#"
                                 onClick={() => setPage('admin')}>
                                  Администрирование
                              </a>
                          </li>
                      )}

                      <li className="nav-item">
                      <a className=" nav-link mx-4 ml-5 text-white" href="#"
                        onClick={() => {}}>
                          Приветствуем вас, {userFio || 'новый пользователь'}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-3">
                <button onClick={toggle}
                        type="button"
                        className="btn btn-primary"
                >
                  {!!cityId && !!cityList[0]
                      ? <><FontAwesomeIcon icon={faMap}/> {cityList.find(i => i.id === cityId).name}</>
                      : <><FontAwesomeIcon icon={faMap} /> {`${(cityList[0] || {}).name } Это ваш город?`}</>
                  }
                </button>
                  <span> </span>
                  <button type="button" className="btn btn-primary"
                          onClick={() =>{
                              unAutentification(loginUserId);
                          }}
                  >
                      Выйти
                  </button>
              </div>
              <div className="col-1"/>
            </div>
          </nav>
    </>

  );
}

export default Navbar;
