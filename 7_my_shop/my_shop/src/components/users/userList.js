import React, {useState, useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faTimesCircle} from '@fortawesome/free-solid-svg-icons'

function UserList(
    {props: {
        loginUserId,
        deleteUser,
        users,
    }},
) {
    const [data, setData] = useState([]);


    useEffect(() => {
            setData(users);
        },
        [users],
    );


    return (

        <div>
            <h2>Просмотр пользователей</h2>

            <table className='table table-bordered table-dark'>
                <thead>
                <tr className=''>
                    <th>№ п/п</th>
                    <th>Имя</th>
                    <th>Логин</th>
                    <th>Права администратора</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    data.map((user, index) => {

                            return <tr className='' key={index}>
                               <td>{user.id}</td>
                               <td>{user.firstName} {user.patroName} {user.lastName}</td>
                               <td>{user.login}</td>
                               <td>{user.accessLevel === 3 ? 'Да' : 'Нет'}</td>
                               <td>
                                   {loginUserId !== user.id && (
                                       <button
                                           type="button"
                                           className="btn btn-danger"
                                           onClick={() => {
                                               deleteUser(user.id);
                                               setData(data.filter(i => i.id !== user.id))
                                               // document.location.reload();

                                               // toggle();
                                           }}
                                       >
                                           <FontAwesomeIcon icon={faTimesCircle}/>
                                       </button>

                                   ) }

                               </td>
                            </tr>

                        })

                }

                </tbody>
            </table>




            <hr/>

        </div>

    );
}

export default UserList;