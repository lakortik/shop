import React, {useState, useEffect} from 'react';
import ChangePassword from '../users/changePassword';
import UserList from '../users/userList';
import AddUser from '../users/addUser';
import ChangeIsLimitForPassword from '../users/changeIsLimitForPassword';

function UserContent(
    {props: {
        loginUserId,
        users,

        addUser,
        exportUsers,
        exportItems,
        changePassword,
        deleteUser,

        setErrorText,
        setMessageText,
        setSuccessText,


        messageModalShow

    }},
) {



    return (

        <div className={'content p-2'}>

            {
                <>

                    <button type="button" className="btn btn-primary  p-2"
                            onClick={() => {
                                exportUsers()
                            }}
                    >
                        Отчет о пользователях
                    </button>
                    <br/>
                    <br/>
                    <button type="button" className="btn btn-primary  p-2"
                            onClick={() => {
                                exportItems()
                            }}
                    >
                        Отчет о товарах
                    </button>

                    <br/>
                    <br/>


                    <UserList props = {{
                        loginUserId,
                        deleteUser,
                        users,
                    }}
                    />

                    <ChangePassword props = {{
                        changePassword,
                        users,
                        setErrorText,
                        setMessageText,
                        setSuccessText,
                        loginUserId,

                    }}
                    />

                    <AddUser props = {{
                        users,

                        setErrorText,
                        setSuccessText,
                        addUser
                        ,
                        messageModalShow
                    }}
                    />

                </>
            }

            <br/>
            <br/>


        </div>



    );
}

export default UserContent;