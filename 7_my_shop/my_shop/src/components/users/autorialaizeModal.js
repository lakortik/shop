import React, {useState} from 'react';
import Modal, { ModalHeader, ModalBody, ModalFooter } from '../users/modal';

function AutorialaizeModal(
    {props: {
        isOpen,
        loginUserId,
        users,


        autentification
    }},
) {

    // const [failCount, setFailCount] = useState(0);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');


    const doAutorialize = () => {


        autentification(login, password);



    };

    return (

        <Modal isOpen={isOpen}>

            <ModalHeader>

                <h3>
                    Войдите в систему
                </h3>

                {/*<button*/}
                {/*    type="button"*/}
                {/*    className="close"*/}
                {/*    aria-label="Close"*/}
                {/*    onClick={toggle}*/}
                {/*>*/}
                {/*    <span aria-hidden="true">&times;</span>*/}
                {/*</button>*/}
            </ModalHeader>
            <ModalBody>

                <label htmlFor="login-input">Логин</label>
                <input className="form-control" type="text" placeholder="Введите логин" id='login-input'
                       value={login}
                       onChange={() => setLogin(document.getElementById('login-input').value)}
                />
                <br/>

                <label htmlFor="password-input">Пароль</label>
                <input className="form-control" type="password" placeholder="Введите пароль" id='password-input'
                       value={password}
                       onChange={() => setPassword(document.getElementById('password-input').value)}
                />

            </ModalBody>
            <ModalFooter>
                {/*<button*/}
                {/*    type="button"*/}
                {/*    className="btn btn-danger"*/}
                {/*    onClick={toggle}*/}
                {/*>*/}
                {/*    Закрыть*/}
                {/*</button>*/}
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={doAutorialize}
                >
                    Войти
                </button>
            </ModalFooter>
        </Modal>
    );
}

export default AutorialaizeModal;