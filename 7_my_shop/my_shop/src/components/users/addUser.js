import React, {useState, useEffect} from 'react';

function AddUser(
    {props: {
        users,

        setErrorText,
        setSuccessText,
        addUser,

        messageModalShow
    }},
) {


    const [newLogin, setNewLogin] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [patroName, setPatroName] = useState('');
    const [accessLevel, setAccessLevel] = useState('');



    // useEffect (()=>{
    //
    // },[key]);


    return  (

        <div>
            <h2>Добавление пользователя</h2>

            <label htmlFor="newLogin-input">Логин нового пользователя </label>
            <input className="form-control" type="text" placeholder="Введите логин" id='newLogin-input'
                value={newLogin}
                onChange={() => setNewLogin(document.getElementById('newLogin-input').value

                )}
            />
            <br/>

            <label htmlFor="firstName-input">Имя нового пользователя </label>
            <input className="form-control" type="text" placeholder="Введите имя" id='firstName-input'
                value={firstName}
                onChange={() => setFirstName(document.getElementById('firstName-input').value

                )}
            />
            <br/>

            <label htmlFor="lastName-input">Фамилия нового пользователя </label>
            <input className="form-control" type="text" placeholder="Введите фамилию" id='lastName-input'
                value={lastName}
                onChange={() => setlastName(document.getElementById('lastName-input').value

                )}
            />
            <br/>

            <label htmlFor="patroName-input">Отчество нового пользователя </label>
            <input className="form-control" type="text" placeholder="Введите отчество" id='patroName-input'
                value={patroName}
                onChange={() => setPatroName(document.getElementById('patroName-input').value

                )}
            />
            <br/>

            <label htmlFor="accessLevel-input">Уровень доступа (3 - администратор)</label>
            <input className="form-control" type="text" placeholder="Введите уровень доступа" id='accessLevel-input'
                value={accessLevel}
                onChange={() => setAccessLevel(document.getElementById('accessLevel-input').value

                )}
            />
            <br/>


            <button type="button" className="btn btn-primary  p-2"
                onClick={() => {
                    const findedUserByLoginId = users.findIndex(user => user.login === newLogin);
                    if (findedUserByLoginId !== -1){
                        setErrorText('Пользователь с таким логином уже есть в системе');
                        return;
                    }
                    //
                    addUser(newLogin, firstName, lastName, patroName, accessLevel);


                }}
            >
                Добавить пользователя
            </button>




            <hr/>

        </div>

    );
}

export default AddUser;