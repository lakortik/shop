import React, {useState, useEffect} from 'react';

function ChangePassword(
    {props: {
        users,
        loginUserId,
        changePassword,

        setErrorText,
        setSuccessText,
    }},
) {


    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPasswordAdditional, setNewPasswordAdditional] = useState('');



    // useEffect (()=>{
    //
    // },[key]);


    return (

        <div>
            <h2>Смена пароля</h2>

            <label htmlFor="oldPassword-input">Старый пароль</label>
            <input className="form-control" type="password"  id='oldPassword-input'
                value={oldPassword}
                onChange={() => setOldPassword(document.getElementById('oldPassword-input').value

                )}
            />
            <br/>

            <label htmlFor="newPassword-input">Новый пароль</label>
            <input className="form-control" type="password" id='newPassword-input'
                value={newPassword}
                onChange={() => setNewPassword(document.getElementById('newPassword-input').value

                )}
            />
            <br/>

            <label htmlFor="newPasswordAdditional-input">Повторно новый пароль</label>
            <input className="form-control" type="password" id='newPasswordAdditional-input'
                value={newPasswordAdditional}
                onChange={() => setNewPasswordAdditional(document.getElementById('newPasswordAdditional-input').value

                )}
            />
            <br/>

            <button type="button" className="btn btn-primary  p-2"
                onClick={() => {
                    if (!(newPassword && newPasswordAdditional)){
                        setErrorText('Введите новый пароль');
                        return;
                    }

                    if (newPassword.length < 4 ){
                        setErrorText('Ограничение. Пароль должен быть длиннее 4 символов');
                        return;
                    }

                    if (newPassword !== newPasswordAdditional ){
                        setErrorText('Новые пароли не совпадают');
                        return;
                    }
                    changePassword(loginUserId, oldPassword, newPassword);
                    // setSuccessText('Пароль успешно изменен');


                }}
            >
                Сменить пароль
            </button>




            <hr/>

        </div>

    );
}

export default ChangePassword;