import React, {useState, useEffect} from 'react';

function ChangeIsLimitForPassword(
    {props: {
        hide,
        users,

        setErrorText,
        setSuccessText,
    }},
) {

    const [loginForLimit, setLoginForLimit] = useState('');


    return !hide && (

        <div>
            <h2>Смена режима ограничений на выбираемые пароли</h2>

            <label htmlFor="loginForLimit-input">Логин пользователя</label>
            <input className="form-control" type="text" placeholder="Введите логин" id='loginForLimit-input'
                   value={loginForLimit}
                   onChange={() => setLoginForLimit(document.getElementById('loginForLimit-input').value

                   )}
            />
            <br/>


            <button type="button" className="btn btn-primary  p-2"
                    onClick={() => {
                        setTimeout(()=>{
                            setSuccessText('');
                        }, 10000);

                        setTimeout(()=>{
                            setErrorText('');
                        }, 5000);

                        const findedUserByLoginId = users.findIndex(user => user.login === loginForLimit);

                        if (findedUserByLoginId === -1){
                            setErrorText('Пользователь с таким логином не найден в системе');
                            return;
                        }

                        setSuccessText(`Пользователю ${users[findedUserByLoginId].name} ${users[findedUserByLoginId].isLimitForPassword 
                            ? 'выключены'
                            : 'включены'
                        } ограничения`);
                        users[findedUserByLoginId].isLimitForPassword = !users[findedUserByLoginId].isLimitForPassword;

                    }}
            >
                Сменить ограничения
            </button>





            <hr/>

        </div>

    );
}

export default ChangeIsLimitForPassword;