import React, {useState, useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faStethoscope, faRubleSign, faMedkit, faListAlt, faCalendarPlus} from '@fortawesome/free-solid-svg-icons'
import Card from "./card";
import UserContent from "./users/userContent";

function Content(
    {props: {
        items,
        sendOrder,
        page,
        loginUserId,
        users,
        accessLevel,

        addUser,
        exportUsers,
        exportItems,
        changePassword,
        changeItemCount,



        deleteUser,

        setErrorText,
        setMessageText,
        setSuccessText,

        messageModalShow


    }},
) {

  const [data, setData] = useState([]);
  const [fio, setFio] = useState('');
  const [address, setAddress] = useState('');
  const [totalSum, setTotalSum] = useState([]);
  const [filters, setFilters] = useState({
        categoryId: 1,
        maxPrice: 99999999999999999999,
        minPrice: 0
  });

  const [sort, setSort] = useState({
      type: 'asc',
      field: 'price'
  });

  useEffect(
        () => {
            setData(items);
        },
        [items],
    );

  const calcTotal = () => {

  };

  useEffect(
() => {
            // calcTotal()
        setTotalSum(data.filter(i => i.byeCount > 0).length > 0 && data.filter(i => i.byeCount > 0)
                      .map(i => i.byeCount * i.price)
                      .reduce((accumulator, currentValue) => accumulator + currentValue)
                  || 0
        )
      },
[page, data],
  );


    let newData ;
    const isMain = page === 'main';
    const isAdmin = page === 'admin' && accessLevel ===3 ;


      newData = data.filter(({
                                   categoryId,
                                   price
                               }) => {
              let ret = categoryId === filters.categoryId || !isMain;
              if (filters.maxPrice) {
                  ret = ret && price <= filters.maxPrice
              }
              if (filters.minPrice) {
                  ret = ret && price >= filters.minPrice
              }
              return ret;
          }
      )
          .sort((a, b) => {
              if (sort.field) {
                  let compareResult = a.price - b.price;

                  return sort.type === 'asc' ? compareResult : compareResult * -1;
              }
          });

    if (!isMain){
        newData = newData.filter(i => i.byeCount > 0).length > 0 && newData.filter(i => i.byeCount > 0)
    }

  return (
      <div className="container-fluid">
          {!isAdmin && <div className="row vh100-80">

              {isMain &&
              <div className="col-2 text-center bgc476DD5">
                  <ul className="navbar-nav">
                      <p className="text-white py-3">Категории</p>
                      <li className="nav-item ">
                          <a
                              className={`nav-link py-3 text-white nav-border ${filters.categoryId === 1 ? 'activeCategory' : ''}`}
                              href="#"
                              id="instruments"
                              onClick={() => setFilters({
                                  ...filters,
                                  categoryId: 1
                              })}
                          >
                              <FontAwesomeIcon icon={faMedkit}/>
                               Препараты
                          </a>
                      </li>
                      <li className="nav-item">
                          <a className={`nav-link py-3 text-white nav-bottom-border ${filters.categoryId === 2 ? 'activeCategory' : ''}`}
                             href="#"
                             id="angle"
                             onClick={() => setFilters({
                                 ...filters,
                                 categoryId: 2
                             })}
                          >
                              <FontAwesomeIcon icon={faStethoscope}/>
                               Медицинские приборы
                          </a>
                      </li>
                      <li className="nav-item">
                          <a className={`nav-link py-3 text-white nav-bottom-border ${filters.categoryId === 3 ? 'activeCategory' : ''}`}
                             href="#"
                             id="pin"
                             onClick={() => setFilters({
                                 ...filters,
                                 categoryId: 3
                             })}
                          >
                              <FontAwesomeIcon icon={faListAlt}/>
                               Препараты по рецепту
                          </a>
                      </li>
                      <li className="nav-item">
                          <a className={`nav-link py-3 text-white nav-bottom-border ${filters.categoryId === 4 ? 'activeCategory' : ''}`}
                             href="#"
                             id="nut"
                             onClick={() => setFilters({
                                 ...filters,
                                 categoryId: 4
                             })}
                          >
                              <FontAwesomeIcon icon={faCalendarPlus}/>
                               Товары по акции
                          </a>
                      </li>
                  </ul>
              </div>
              }
              <div className="col-1"/>
              <div className={`${isMain ? 'col-8' : 'col-10'}`}>
                  <div className="row py-4">
                      <div className="col-6 ">
                          <div
                              className="p-2 "
                          >
                              <p>Фильтрация</p>
                              <hr/>

                              <p>Цена</p>

                              <div className="row">

                                  <div className="col-6 ">
                                      <label htmlFor="minPrice">от:</label>
                                      <br/>
                                      <input type="number"
                                             id="minPrice"
                                             name="minPrice"
                                             className="w100"
                                             onChange={({target: {valueAsNumber}}) => setFilters({
                                                 ...filters,
                                                 minPrice: valueAsNumber
                                             })}
                                      />
                                  </div>
                                  <div className="col-6 ">
                                      <label htmlFor="maxPrice">до:</label>
                                      <br/>
                                      <input type="number"
                                             id="maxPrice"
                                             name="maxPrice"
                                             className="w100"
                                             onChange={({target: {valueAsNumber}}) => setFilters({
                                                 ...filters,
                                                 maxPrice: valueAsNumber
                                             })}
                                      />
                                  </div>
                              </div>

                          </div>
                      </div>
                      <div className="col-6 ">
                          <div
                              className="p-2 custom-border-1"
                          >
                              <p>Сортировка</p>
                              <hr/>
                              <select className="w250px"
                                      id="sort"
                                      onChange={({target: {value}}) => setSort({
                                          type: value === 'priceAsc' ? 'asc' : 'desc',
                                          field: 'price'
                                      })}
                              >
                                  <option value="priceAsc">Цена. по возрастанию</option>
                                  <option value="priceDesc">Цена. по убыванию</option>
                              </select>
                          </div>
                      </div>
                  </div>
                  <hr/>
                  <div id="itemPojos-container"
                       className="row align-center"
                  >
                      {newData.length > 0
                          ? newData.map((item, key) => <Card
                              key={key}
                              props={{
                                  item: item,
                                  changeItemCount,
                                  setTotalSum,
                                  totalSum,
                                  isMain
                              }}
                          />)
                          : <h3>Ничего не найдено</h3>
                      }
                  </div>

                  {!isMain &&
                  <>

                      <div className='col-12'>
                          <h2>Итого: {totalSum} <FontAwesomeIcon icon={faRubleSign}/>
                          </h2>
                      </div>

                      <div className="col-6 mt-4">
                          <label htmlFor="fio">ФИО:</label>
                          <br/>
                          <input type="text"
                                 id="fio"
                                 name="fio"
                                 className="w100"
                                 onChange={({target: {value}}) => setFio(value)}

                          />
                      </div>
                      <div className="col-6 mt-4">
                          <label htmlFor="address">Адрес:</label>
                          <br/>
                          <input type="text"
                                 id="address"
                                 name="address"
                                 className="w100"
                                 onChange={({target: {value}}) => setAddress(value)}
                          />
                      </div>

                      <div className="col-2 offset-10 mt-4">
                          <button type="button" className="btn btn-success"
                                  onClick={() => sendOrder(fio, address)}
                          >
                              Оформить покупку!
                          </button>
                      </div>
                      <br/>
                      <br/>
                      <br/>
                      <br/>
                      <br/>
                      <br/>
                      <br/>
                      <br/>

                  </>
                  }


              </div>

          </div>
          }

          {isAdmin && <div className="row vh100-80">
              <div className="col-8 offset-2">
                  <UserContent
                      props = {{
                          loginUserId,
                          users,

                          addUser,
                          exportUsers,
                          exportItems,
                          changePassword,
                          deleteUser,
                          setErrorText,
                          setMessageText,
                          setSuccessText,

                          messageModalShow
                      }}
                  />
              </div>

            </div>
          }
      </div>

  );
}

export default Content;