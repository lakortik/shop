//Modal component
import React, {useEffect, useState} from 'react';
import Modal, { ModalHeader, ModalBody, ModalFooter } from '../components/modal';

function CityModal(
    {props: {
        isOpen,

        errorText,
        messageText,
        successText,

        toggle,
        show
    }},
) {

    useEffect (()=>{
        if (errorText||messageText||successText)
        {
            show();

        }
    },[errorText, messageText, successText]);

    return (
        <Modal isOpen={isOpen}>
            <ModalHeader>
                {errorText && (<h3>Ошибка</h3>)}
                {successText && (<h3>Успешно!</h3>)}
                {messageText && (<h3>Сообщение</h3>)}
                <button
                    type="button"
                    className="close"
                    aria-label="Close"
                    onClick={toggle}
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </ModalHeader>
            <ModalBody>
                {errorText && (<p>{errorText}</p>)}
                {successText && (<p>{successText}</p>)}
                {messageText && (<p>{messageText}</p>)}

            </ModalBody>
            <ModalFooter>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={toggle}
                >
                    Закрыть
                </button>

            </ModalFooter>
        </Modal>
    );
}

export default CityModal;