import React, {useState, useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faAngleLeft, faRubleSign, faRulerHorizontal, faScrewdriver, faWrench} from '@fortawesome/free-solid-svg-icons'


function Card(
    {props: {
        item: {
            id,
            name,
            price,
            description,
            labels,
            byeCount
        },
        changeItemCount,
        totalSum,
        setTotalSum,
        isMain
    }},

) {


  return (
      <div className={`${isMain ? 'col-6' : 'col-12'}`}>
          <div className="card my-3">
              <div className="card-body">
                  <h5 className="card-title">{name}</h5>
                  <p className="card-text">{description}</p>
                  <p><b>{price.toFixed(2)} <FontAwesomeIcon icon={faRubleSign} /> </b></p>
                  <div /*id="item-buttons-${id}"*/>
                      {byeCount
                          ? <div className="btn-group" role="group" aria-label="Basic example">
                              <button type="button"
                                      className="btn btn-danger w40px"
                                        onClick={() => {
                                            changeItemCount(id, 'deleteItem');
                                            setTotalSum(totalSum - price);

                                        }}
                              >
                                  -
                              </button>

                              <button type="button"
                                      className="btn btn-white w50px"
                              >
                                  {byeCount}
                              </button>

                              <button type="button"
                                      className="btn btn-success w40px"
                                      onClick={() => {
                                          changeItemCount(id, 'addItem');
                                          setTotalSum(totalSum + price);
                                      }}

                              >
                                  +
                              </button>
                          </div>
                          : <button type="button"
                                    className="btn btn-primary"
                                    onClick={() => changeItemCount(id, 'addItem')}
                          >
                              Добавить в корзину
                          </button>
                      }
                  </div>

              </div>
          </div>
      </div>

  );
}



export default Card;