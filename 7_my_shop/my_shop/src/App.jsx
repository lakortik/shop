import React, { useState } from 'react';
import './App.css';
import Navbar from './components/navbar';
import Content from "./components/content.jsx";
import CityModal from "./components/cityModal";
import MessageModal from "./components/messageModal";
import * as axios from 'axios';

import UserContent from "./components/users/userContent";
import Header from "./components/users/header";
import AutorialaizeModal from "./components/users/autorialaizeModal";



function User(id, firstName, lastName, patroName, login, accessLevel) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.patroName = patroName;
    this.login = login;
    this.accessLevel = accessLevel;
}


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            accessLevel: null,

            messageError: null,
            messageText: null,
            messageSuccess: null,

            users: [],
            loginUserId: null,
            errorText: null,
            successText: null,
            autorizeModalShow: null,
            cityList: [],
            page: null,
            messageModalShow: false,
            cityModalShow: false,
            cityId: !!this.getCookieValueByName("cityId")
                        ? parseInt(this.getCookieValueByName("cityId"))
                        : null
        };
    }



    getCookieValueByName (name) {
        return !!document.cookie.split(";")
            && !!document.cookie.split(";").map(i => i.trim()).filter(i => i.startsWith(name))[0]
            && document.cookie.split(";").map(i => i.trim()).filter(i => i.startsWith(name))[0].split("=")[1];
    }

    getUsers () {
        axios.get(
            'http://localhost:8080/api/users',
            {
                withCredentials: true,
                credentials: 'include',
            }
        )
            .then(response => this.setState({
                    ...this.state,
                    users: response.data.map(userFromApi => new User(
                        userFromApi.id,
                        userFromApi.firstName,
                        userFromApi.lastName,
                        userFromApi.patroName,
                        userFromApi.login,
                        userFromApi.accessLevel
                        )
                    ),
                })
            );
    }


    isAutorialize () {
        axios.get(
            'http://localhost:8080/api/isAutorize',
            {
                withCredentials: true,
                credentials: 'include',
            }
        )
            .then(response => {
                if (response.data.id) {
                    this.setState({
                        ...this.state,
                        autorizeModalShow: false,
                        loginUserId: response.data.id,
                        accessLevel: response.data.accessLevel,
                        page: response.data.accessLevel === 3 ? 'admin' : 'main',
                    })
                } else {
                    this.setState({
                        ...this.state,
                        autorizeModalShow: true,
                    })

                }

            });

    }

    componentDidMount() {
        axios.get(
                'http://localhost:8080/api/items',
                {
                    withCredentials: true,
                    credentials: 'include',
                }
            )
            .then(response => this.setState({
                    ...this.state,
                    items: response.data,
                })
            );
        this.getUsers();

        this.isAutorialize();

        axios.get(
                'http://localhost:8080/api/city',
                {
                    withCredentials: true,
                    credentials: 'include',
                }
            )
            .then(response => this.setState({
                    ...this.state,
                    cityList: response.data,
                })
            );
    }

    changeItemCount (itemId, operationType) {

        axios.get(
            `http://localhost:8080/api/changeItemCountCros?itemId=${itemId}&operationType=${operationType}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
                const newItems = this.state.items;

                const dataIndex = newItems.findIndex(i => i.id === itemId);
                newItems[dataIndex].byeCount = parseInt(response.data);

                this.setState({
                    ...this.state,
                    items: newItems,
                })
            });
    };

    sendOrder (fio, address) {
        axios.get(
            `http://localhost:8080/api/sendOrder?fio=${fio}&address=${address}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
            this.setState({
                ...this.state,
                page: 'main',
            });
        });

    };

    changePassword (id, password, newPassword) {
        axios.get(
            `http://localhost:8080/api/changePassword?id=${id}&password=${password}&newPassword=${newPassword}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
            if (response.data === -1){
                this.setErrorText('Неправильно введен текущий пароль');
                return;
            } else {
                this.setSuccessText('Пароль успешно изменен');
                // this.setState({
                //     ...this.state,
                //     autorizeModalShow: !this.state.autorizeModalShow,
                //     loginUserId: response.data,
                // });
                // this.isAutorialize();
            }
        });

    };

    addUser (newLogin, firstName, lastName, patroName, accessLevel) {
        axios.get(
            `http://localhost:8080/api/addUser?newLogin=${newLogin}&firstName=${firstName}&lastName=${lastName}&patroName=${patroName}&accessLevel=${accessLevel}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
                this.setSuccessText(`Пользователь успешно добавлен. Его сгенерированный пароль: ${response.data}`);
this.getUsers();
                // this.setState({
                //     ...this.state,
                //     autorizeModalShow: !this.state.autorizeModalShow,
                //     loginUserId: response.data,
                // });
                // this.isAutorialize();
        });

    };


    exportUsers () {
        window.open('http://localhost:8080/api/exportUsers');
    };

    exportItems () {
        window.open('http://localhost:8080/api/exportItems');
    };

    autentification (login, password) {
        const setErrorText = text=> {
            this.setState({
                ...this.state,
                errorText: text,
            });

            setTimeout( ()=>{
                this.setState({
                    ...this.state,
                    errorText: null,
                });
            }, 3000 );
        };

        const setSuccessText = text=> {
            this.setState({
                ...this.state,
                successText: text,
            });
            setTimeout( ()=>{
                this.setState({
                    ...this.state,
                    successText: null,
                });
            }, 3000 );
        };

        if (!login){
            setErrorText('Введите логин');

            return;
        }
        axios.get(
            `http://localhost:8080/api/autentification?login=${login}&password=${password}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
            if (response.data === -1){
                setErrorText('Неправильный логин или пароль');
                return;
            } else {
                setSuccessText('Вы успешно авторизовались');
                this.setState({
                    ...this.state,
                    autorizeModalShow: !this.state.autorizeModalShow,
                    loginUserId: response.data,
                });
                this.isAutorialize();

            }
        });

    };

    unAutentification = id => {
        axios.get(
            `http://localhost:8080/api/unAutentification?id=${id}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
            this.setState({
                ...this.state,
                autorizeModalShow: !this.state.autorizeModalShow,
                loginUserId: null,
            });
        });
    };

    deleteUser = id => {
        axios.get(
            `http://localhost:8080/api/deleteUser?id=${id}`,
            {
                withCredentials: true,
                credentials: 'include',
            }
        ).then(response => {
            this.setSuccessText("Пользователь успешно удален");
        });
    };

    messageModalToggle = () => {
        this.setState({
            ...this.state,
            messageModalShow: !this.state.messageModalShow,
            messageText: null,
            messageError: null,
            messageSuccess: null,
        });
    };

    messageModalShow = () => {
        this.setState({
            ...this.state,
            messageModalShow: true,
        });
    };



    setMessageText = (text) =>{
        this.setState({
            ...this.state,
            messageText: text,
        });
    };

    setSuccessText = (text) =>{
        this.setState({
            ...this.state,
            messageSuccess: text,
        });
    };

    setErrorText = (text) =>{
        this.setState({
            ...this.state,
            messageError: text,
        });
    };

    render() {
        // region
        const toggleAutorizeModal = () => {
            this.setState({
                ...this.state,
                autorizeModalShow: !this.state.autorizeModalShow,
            });
        };

        const setLoginUserId = id => {
            this.state.loginUserId = id;
        };

        const toggle = () => {
            this.setState({
                ...this.state,
                cityModalShow: !this.state.cityModalShow,
            });
        };

        const setPage = (page) => {
            this.setState({
                ...this.state,
                page: page,
            });
        };
        const getUserFio = () => {
            const user = this.state.users.find(user => user.id === this.state.loginUserId) ;
            if (user === null || user === undefined) {
                return '';
            }

            return `${user.firstName} ${user.lastName.substr(0, 1)}. ${user.patroName.substr(0, 1)}.`;
        };

        // endregion

        return (
            <div className="App">

                {this.state.autorizeModalShow && (
                    //region autorize
                <>
                    {this.state.errorText &&(
                        <div className="alert alert-danger " role="alert">
                            {this.state.errorText}
                        </div>
                    )}
                    {this.state.successText &&(
                        <div className="alert alert-success " role="alert">
                            {this.state.successText}
                        </div>
                    )}
                    <AutorialaizeModal
                        props = {{
                            isOpen: this.state.autorizeModalShow,
                            loginUserId: this.state.loginUserId,
                            users: this.state.users,

                            setLoginUserId,
                            autentification: (login, password) => this.autentification(login, password),
                            toggle: toggleAutorizeModal,
                        }}
                    />
                </>
                    //endregion

                )}



                {!this.state.autorizeModalShow && (
                <>

                    <CityModal
                        props = {{
                            isOpen: this.state.cityModalShow,
                            cityList: this.state.cityList,
                            cityId: this.state.cityId,
                            toggle,
                        }}
                    />

                    <MessageModal
                        props = {{
                            isOpen: this.state.messageModalShow,
                            errorText: this.state.messageError,
                            messageText:this.state.messageText,
                            successText:this.state.messageSuccess,


                            toggle: this.messageModalToggle,
                            show: this.messageModalShow,
                        }}
                    />

                    <Navbar
                        props = {{
                            cityId: this.state.cityId,
                            cityList: this.state.cityList,
                            loginUserId: this.state.loginUserId,
                            accessLevel: this.state.accessLevel,
                            userFio: getUserFio(),

                            unAutentification: (id) => this.unAutentification(id),

                            toggle,
                            setPage
                        }}
                    />


                    <Content
                        props = {{
                            items: this.state.items,
                            page: this.state.page,
                            loginUserId: this.state.loginUserId,
                            users: this.state.users,

                            accessLevel: this.state.accessLevel,


                            addUser:  (newLogin, firstName, lastName, patroName, accessLevel) => this.addUser (newLogin, firstName, lastName, patroName, accessLevel),
                            changePassword: (id, password, newPassword) => this.changePassword(id, password, newPassword),
                            deleteUser: (id) => this.deleteUser(id),
                            exportUsers: () => this.exportUsers(),
                            exportItems: () => this.exportItems(),
                            setErrorText: (text) => this.setErrorText(text),
                            setMessageText: (text) => this.setMessageText(text),
                            setSuccessText: (text) => this.setSuccessText(text),
                            messageModalShow: this.messageModalShow,



                            changeItemCount: (itemId, operationType) => this.changeItemCount(itemId, operationType),
                            sendOrder: (fio, address) => this.sendOrder(fio, address)
                        }}
                    />

                </>
                )}
            </div>
        );


    }
}

export default App;
